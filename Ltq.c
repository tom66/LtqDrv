﻿/***************************************************************************************
* AUTHOR : �ͷ��
* DATE   : 2020-07
****************************************************************************************/
#include "Ltq.h"

pMiProcessLoaderEntry g_MiProcessLoaderEntry = NULL;
UCHAR g_szMiProcessLoaderEntry[41] = { 0x48, 0x89, 0x5c, 0x24, 0x08, 0x48, 0x89, 0x6c, 0x24, 0x18, 0x48, 0x89, 0x74, 0x24, 0x20, 0x57,
0x41, 0x54, 0x41, 0x55, 0x41, 0x56, 0x41, 0x57, 0x48, 0x83, 0xec, 0x30, 0x48, 0x8b, 0xf9, 0x44,
0x8b, 0xf2, 0xbb, 0x01, 0x00, 0x00, 0x00, 0x48, 0x8d };

HANDLE obHandle;
ULONG64 protectPid[20];//受保护的进程

/**
�������
*/
NTSTATUS DriverEntry(PDRIVER_OBJECT driver, PUNICODE_STRING path)
{
	WSKStartup();
	
	DbgPrint("�ͷ�ࣺ�����Ѽ���,·��:%wZ\n", path);

	driver->DriverUnload = DriverUnload;

	CreateDevice(driver);

	driver->MajorFunction[IRP_MJ_CREATE] = DispatchCreate;
	driver->MajorFunction[IRP_MJ_CLOSE] = DispatchClose;
	driver->MajorFunction[IRP_MJ_DEVICE_CONTROL] = DriverIrpCtl;

	//����ģ����ػص�֪ͨ������ֹ����tesxnginx.sys����
	//PsSetLoadImageNotifyRoutine((PLOAD_IMAGE_NOTIFY_ROUTINE)LoadImageNotifyRoutine);

	PLDR_DATA_TABLE_ENTRY64 ldrDataTable;
	ldrDataTable = (PLDR_DATA_TABLE_ENTRY64)driver->DriverSection;
	ldrDataTable->Flags |= 0x20;  //��MmVerifyCallbackFunction

	ProtectProcess(TRUE);
	return STATUS_SUCCESS;
}
/**
��������
*/
void DriverUnload(PDRIVER_OBJECT driver)
{
	WSKCleanup();
	if (driver->DeviceObject)
	{
		UNICODE_STRING SymbolName;
		RtlInitUnicodeString(&SymbolName, SYMBOLNAME);

		IoDeleteSymbolicLink(&SymbolName);
		IoDeleteDevice(driver->DeviceObject);
	}
	//ж��ģ�����֪ͨ�ص�
	PsRemoveLoadImageNotifyRoutine((PLOAD_IMAGE_NOTIFY_ROUTINE)LoadImageNotifyRoutine);

	ProtectProcess(FALSE);
	DbgPrint("�ͷ�ࣺ������ж��\n");
}
NTSTATUS
DispatchCreate(IN PDEVICE_OBJECT pDevObj, IN PIRP pIrp)
{
	pIrp->IoStatus.Status = STATUS_SUCCESS;
	pIrp->IoStatus.Information = 0;
	IoCompleteRequest(pIrp, IO_NO_INCREMENT);
	return STATUS_SUCCESS; //STATUS_SUCCESS;
}


NTSTATUS
DispatchClose(IN PDEVICE_OBJECT pDevObj, IN PIRP pIrp)
{
	pIrp->IoStatus.Status = STATUS_SUCCESS;
	pIrp->IoStatus.Information = 0;
	IoCompleteRequest(pIrp, IO_NO_INCREMENT);
	return STATUS_SUCCESS; //STATUS_SUCCESS;
}
/**
��������
*/
NTSTATUS CreateDevice(PDRIVER_OBJECT driver)
{
	NTSTATUS status = STATUS_SUCCESS;
	PDEVICE_OBJECT device = NULL;
	UNICODE_STRING DeviceName;

	RtlInitUnicodeString(&DeviceName, DEVICENAME);

	status = IoCreateDevice(
		driver,
		sizeof(driver->DriverExtension),
		&DeviceName,
		FILE_DEVICE_UNKNOWN,
		FILE_DEVICE_SECURE_OPEN,
		FALSE,
		&device
	);

	if (status == STATUS_SUCCESS)
	{
		UNICODE_STRING SymbolName;
		RtlInitUnicodeString(&SymbolName, SYMBOLNAME);

		status = IoCreateSymbolicLink(&SymbolName, &DeviceName);

		if (status != STATUS_SUCCESS)
		{
			DbgPrint("�ͷ�ࣺ������������ʧ��\n");
			IoDeleteDevice(device);
		}
	}

	DbgPrint("�ͷ�ࣺ�����豸�Ѵ���\n");
	return status;
}



/**
����ͨѶ
*/
NTSTATUS DriverIrpCtl(PDEVICE_OBJECT device, PIRP pirp)
{
	PIO_STACK_LOCATION stack;

	stack = IoGetCurrentIrpStackLocation(pirp);

	ULONG uIoControlCode = stack->Parameters.DeviceIoControl.IoControlCode;
	pirp->IoStatus.Information = stack->Parameters.DeviceIoControl.OutputBufferLength;

	//传输结构

	LtqData* LtqData = pirp->AssociatedIrp.SystemBuffer;
	

	//DbgPrint("愣头青：PID:%d 地址:%x, 数据:%x  大小:%d  密钥:%d\n",LtqData->pid, LtqData->address, LtqData->data, LtqData->size, LtqData->key);


		switch (uIoControlCode)
		{

		case CHECK_CODE:
		{
			//DbgPrint("�ͷ�ࣺ��֤\n");
			lock = GetLock(1);
			if ((ULONG64)lock == LtqData->key) {
				DbgPrint("愣头青：第一层验证通过！\n");
				if (Login(LtqData)) {
					check = TRUE;
					DbgPrint("愣头青：第二层验证通过！\n");
				}
				//移除驱动模块
				//DbgPrint("愣头青：处理驱动痕迹！");
				//RemoveSystemModule(device->DriverObject);
			}
			if (check) {
				DbgPrint("愣头青：验证通过！\n");
			}
			else {
				DbgPrint("愣头青：验证失败！\n");
			}
			break;
		}

		case READ_CODE:
		{
			if (check) {
				//DbgPrint("愣头青：读内存\n");
				ReadMemory(LtqData);
			}
			break;
		}

		case WRITE_CODE:
		{
			if (check) {
				DbgPrint("�ͷ�ࣺд�ڴ�\n");
				WriteMemory(LtqData);
			}
			break;
		}

		case MODULE_CODE:
		{
			if (check) {
				DbgPrint("�ͷ�ࣺ��ģ��\n");
				GetModule(LtqData);
			}
			break;
		}

		case ALLOC_CODE:
		{
			if (check) {
				DbgPrint("�ͷ�ࣺ�����ڴ�\n");
				AllocMemory(LtqData);
			}
			break;
		}

		case FREE_CODE:
		{
			if (check) {
				DbgPrint("�ͷ�ࣺ�ͷ��ڴ�\n");
				FreeMemory(LtqData);
			}
			break;
		}
		case OPEN_PROTECT_CODE:
		{
			if (check) {
				DbgPrint("愣头青：保护进程\n"); 
				OpenProtect(LtqData);
			}
			break;
		}
		case OFF_PROTECT_CODE:
		{
			if (check) {
				DbgPrint("愣头青：关闭保护\n");
				OffProtect(LtqData);
			}
			break;
		}
		case MODULEFUNC_CODE:
		{
			if (check) {
				DbgPrint("愣头青：读模块函数\n");
				GetModuleFunc(LtqData);
			}
			break;
		}
	}

	pirp->IoStatus.Status = STATUS_SUCCESS;
	IoCompleteRequest(pirp, IO_NO_INCREMENT);

	return STATUS_SUCCESS;
}



int GetLock(int type)
{
	int check = 66;
	if (type == 1) {
		check = 12 * 21;
	}
	else if (type == 2) {
		check = 13 * 31;
	}
	LARGE_INTEGER GelinTime = { 0 };
	LARGE_INTEGER LocalTime = { 0 };
	TIME_FIELDS NowFields;

	KeQuerySystemTime(&GelinTime);
	ExSystemTimeToLocalTime(&GelinTime, &LocalTime);
	RtlTimeToTimeFields(&LocalTime, &NowFields);
	int day = NowFields.Day;
	int month = NowFields.Month;
	int ret = 0;
	ret = (9215 * day - check) * month;
	return ret;
}

BOOLEAN Login(LtqData* data)
{
	BOOLEAN ret = FALSE;
	char* Data = (char*)ExAllocatePool(NonPagedPool, 1000);
	char* Data1 = (char*)ExAllocatePool(NonPagedPool, 100);

	__try {
		char Buffer[1000] = "";
		memset(Data, 0, 1000);
		memset(Data1, 0, 100);

		char username[100] = "";
		char password[100] = "";
		RtlCopyMemory(username, data->address, strlen((char*)((PVOID)data->address)));
		RtlCopyMemory(password, data->data, strlen((char*)((PVOID)data->data)));
		Data1 = join("username=", username);
		Data = join("&password=", password);
		Data = join(Data1, Data);
		Data = join(Data, "&Ver=1.0\n");
		Post(47, 107, 255, 5, "47.107.255.5:666", "ltq/getKey1", 666, Data, "", Buffer, 1000);

		//DbgPrint("�ͷ�ࣺ���������:%s\n\n", Data);
		int size = strrchr(Buffer, '}') - strrchr(Buffer, ':') - 1;//strlen(strrchr(Buffer, ':')+1)-2;
		strncpy(Buffer, strrchr(Buffer, ':') + 1, size);
		Buffer[size] = 0;

		//DbgPrint("�ͷ�ࣺ ������KEY:%s SIZE:%d\n\n", Buffer, size);
		int key = GetLock(2);

		char token[10] = "";
		sprintf(token, "%d", key);

		DbgPrint("�ͷ�ࣺ ������KEY:%s ����KEY:%d\n\n", Buffer, key);
		if (_stricmp(Buffer, token)==0) {
			ret = TRUE;
		}
	}
	__except (1) {

		DbgPrint("�ͷ�ࣺ��¼�쳣�� \n\n");
		ret = FALSE;
	}
	ExFreePool(Data);
	ExFreePool(Data1);
	return ret;
}

/**
���ڴ�
*/
BOOLEAN ReadMemory(LtqData* data) {
	BOOLEAN bRet = TRUE;
	PEPROCESS process = NULL;

	if (NT_SUCCESS(PsLookupProcessByProcessId((HANDLE)data->pid, &process))) {

		KAPC_STATE stack = { 0 };
		KeStackAttachProcess(process, &stack);

		//判断地址是否有效
		if (MmIsAddressValid(data->address)) {
			//DbgPrint("愣头青：地址校验通过\n");
			if (PsGetProcessExitStatus(process) == 0x103)
			{
				//DbgPrint("愣头青：开始读取内存\n");
				__try {
					//DbgPrint("愣头青：开始读取内存 \n");
					RtlCopyMemory(data, data->address, data->size);
					//DbgPrint("愣头青：读取内存成功!\n");
				
				}
				__except (1)
				{
					__try {
						DbgPrint("愣头青：读取失败，切入CR3读取！ \n");
						ULONG64 OldCr3;
						ULONG64 NewCr3;
						ULONG64 OldCr0;

						NewCr3 = *(PULONG64)((ULONG64)process + 0x28);
						OldCr3 = __readcr3();
						OldCr0 = __readcr0();

						_disable();
						__writecr3(NewCr3);
						__writecr0(OldCr0 & 0xfffffffffffeffff);
						_enable();
						RtlCopyMemory(data, data->address, data->size);
						DbgPrint("愣头青：CR3读取成功！ \n");
						_disable();
						__writecr3(OldCr3);
						__writecr0(OldCr0);
						_enable();
					}
					__except (1) {
						DbgPrint("愣头青：读取内存失败2\n");
						bRet = FALSE;
					}
				}
			}
		}

		KeUnstackDetachProcess(&stack);
		ObDereferenceObject(process);
	}

	return bRet;
}

/**
д�ڴ�
*/
BOOLEAN WriteMemory(LtqData* data) {
	BOOLEAN bRet = TRUE;
	PEPROCESS process = NULL;

	if (NT_SUCCESS(PsLookupProcessByProcessId((HANDLE)data->pid, &process))) {

		KAPC_STATE stack = { 0 };
		KeStackAttachProcess(process, &stack);
		//�жϵ�ַ�Ƿ���Ч
		if (MmIsAddressValid(data->address)) {
			DbgPrint("�ͷ�ࣺ��ַУ��ͨ��\n");

			PVOID buff = NULL;
			buff = ExAllocatePoolWithTag(NonPagedPool, data->size,'Sys');
				if(buff == NULL){
					KeUnstackDetachProcess(&stack);
					ObDereferenceObject(process);
					return FALSE;
				}
				__try
				{
					RtlCopyMemory(buff, (PVOID)((ULONG64)data + sizeof(LtqData)), data->size);
				}
				__except (EXCEPTION_EXECUTE_HANDLER)
				{
					KeUnstackDetachProcess(&stack);
					ObDereferenceObject(process);
					return FALSE;
				}


				PMDL MDL = NULL;
				PVOID NewAddress = NULL;
				__try {
					MDL = IoAllocateMdl(data->address, data->size, FALSE, FALSE, NULL);
					if (MDL)
					{
						MmBuildMdlForNonPagedPool(MDL);
						MmProbeAndLockPages(MDL, 0, 0);
						NewAddress = MmMapLockedPagesSpecifyCache(MDL, NULL, MmCached, NULL, NULL, HighPagePriority);
						if (NewAddress)
						{
							DbgPrint("愣头青：开始写入内存 \n");
							RtlCopyMemory(NewAddress, buff, data->size);
							DbgPrint("愣头青：写入内存成功!\n");
						}
					}
					if (NewAddress)
						MmUnmapLockedPages(NewAddress, MDL);
					if (MDL)
					{
						//if (MDL->MdlFlags != 2)
						MmUnlockPages(MDL);
						IoFreeMdl(MDL);
					}
				}
				__except (EXCEPTION_EXECUTE_HANDLER) {
					DbgPrint("愣头青：写入内存失败2!\n");
				}
				ExFreePool(buff);
						//ULONG_PTR ulOldProtect = 0;
						//PVOID     ulStart = data->address;
						//ULONG_PTR ulRegionSize = data->size;
						//关闭分页保护
						//KIRQL irql = WPOF();
						//ZwProtectVirtualMemory(process, &ulStart, &ulRegionSize, PAGE_READWRITE, &ulOldProtect);
						//if (NT_SUCCESS(MmCopyVirtualMemory(IoGetCurrentProcess(), (PVOID)((ULONG64)data + sizeof(LtqData)), process, (PVOID)(data->address), data->size, KernelMode, &Size))) {
						//	DbgPrint("愣头青：写入内存成功\n");
						//}
						//else {
						//	DbgPrint("愣头青：写入内存失败1\n");
						//	bRet = FALSE;
						//}

						//恢复分页保护
						//ZwProtectVirtualMemory(process, &ulStart, &ulRegionSize, ulOldProtect, NULL);
						//WPON(irql);

				/*
				PMDL MDL = NULL;
				PVOID NewAddress = NULL;
				__try {
					MDL = IoAllocateMdl((PVOID)(data->address), (ULONG)data->size, FALSE, FALSE, NULL);
					if (MDL)
					{
						MmBuildMdlForNonPagedPool(MDL);
						MmProbeAndLockPages(MDL, 0, 0);
						NewAddress = MmMapLockedPagesSpecifyCache(MDL, NULL, MmCached, NULL, NULL, HighPagePriority);
						if (NewAddress)
						{
							DbgPrint("愣头青：开始写入内存 \n");
							RtlCopyMemory(NewAddress, buff, (ULONG)data->size);
							DbgPrint("愣头青：写入内存成功!\n");
						}
					}
					if (NewAddress)
						MmUnmapLockedPages(NewAddress, MDL);
					if (MDL)
					{
						if (MDL->MdlFlags != 2)
							MmUnlockPages(MDL);
						IoFreeMdl(MDL);
					}
				}
				*/


				/*
				__try {
					SIZE_T Size;
					ULONG64 OldCr3;
					ULONG64 NewCr3;
					ULONG64 OldCr0;
					//KIRQL irql = WPOF();
					
					NewCr3 = *(PULONG64)((ULONG64)process + 0x28);
					OldCr3 = __readcr3();
					OldCr0 = __readcr0();

					_disable();
					__writecr3(NewCr3);
					__writecr0(OldCr0 & 0xfffffffffffeffff);
					_enable();

					if (NT_SUCCESS(MmCopyVirtualMemory(IoGetCurrentProcess(), buff, process, (PVOID)(data->address), data->size, KernelMode, &Size))) {
						DbgPrint("�ͷ�ࣺд���ڴ�ɹ�!\n");
					}
					else {
						DbgPrint("�ͷ�ࣺд���ڴ�ʧ��1\n");
						bRet = FALSE;
					}
					_disable();
					__writecr3(OldCr3);
					__writecr0(OldCr0);
					_enable();
					//WPON(irql);
				}
				*/
				
			
		}

		KeUnstackDetachProcess(&stack);
		ObDereferenceObject(process);

	}
	return bRet;
}

//Windows �ر��ڴ�д�����Ĵ��룺
KIRQL WPOF()
{
	KIRQL irql = KeRaiseIrqlToDpcLevel();
	UINT64 cr0 = __readcr0();
	cr0 &= 0xfffffffffffeffff;
	__writecr0(cr0);
	_disable();
	return irql;
}
//Windows ���ڴ�д�����Ĵ��룺
VOID WPON(KIRQL irql)
{
	UINT64 cr0 = __readcr0();
	cr0 |= 0x10000;
	_enable();
	__writecr0(cr0);
	KeLowerIrql(irql);
}

/**
��ģ��
*/
BOOLEAN GetModule(LtqData* data) {
	BOOLEAN bRet = TRUE;

	//PREAD_WRITE_INFO GetModue = (PREAD_WRITE_INFO)pIoBuffer;
	ANSI_STRING AnsiBuffer = { 0 };
	UNICODE_STRING ModuleName = { 0 };
	AnsiBuffer.Buffer = (PVOID)data->address;
	AnsiBuffer.Length = AnsiBuffer.MaximumLength = (USHORT)strlen((PVOID)data->address);
	RtlAnsiStringToUnicodeString(&ModuleName, &AnsiBuffer, TRUE);//ת��
	DbgPrint("�ͷ�ࣺ ModuleName:%wZ\n", &ModuleName);
	ULONG64 module = GetProcessModuleBase64((ULONG)data->pid, ModuleName);
	if (module <= 0) {
		DbgPrint("�ͷ�ࣺö��64PEBʧ��,��ʼö��32PEB��\n");
		module = GetProcessModuleBase32((ULONG)data->pid, ModuleName);
	}
	/*if (module <= 0) {
		DbgPrint("�ͷ�ࣺö��32PEBʧ��,��ʼ����ö�������ڴ档\n");
		module = GetProcessModuleBaseAll((ULONG)data->pid, ModuleName);
	}*/
	*(PVOID*)data = module;
	RtlFreeUnicodeString(&ModuleName);// �ͷ��ڴ�

	return bRet;
}

/**
��������ַ
*/
BOOLEAN GetModuleFunc(LtqData* data) {
	BOOLEAN bRet = TRUE;

	//PREAD_WRITE_INFO GetModue = (PREAD_WRITE_INFO)pIoBuffer;
	ANSI_STRING AnsiBuffer = { 0 };
	UNICODE_STRING FuncName = { 0 };
	AnsiBuffer.Buffer = (PVOID)data->data;
	AnsiBuffer.Length = AnsiBuffer.MaximumLength = (USHORT)strlen((PVOID)data->data);
	RtlAnsiStringToUnicodeString(&FuncName, &AnsiBuffer, TRUE);//ת��
	DbgPrint("�ͷ�ࣺ ������:%wZ\n", &FuncName);


	ULONG64 funcAddr = GetModuleFuncBase64(data->pid,data->address, FuncName);
	if (funcAddr <= 0) {
		DbgPrint("�ͷ�ࣺö��64ģ��PEʧ��,��ʼö��32ģ��PE��\n");
		funcAddr = GetModuleFuncBase32(data->pid, data->address, FuncName);
	}
	*(PVOID*)data = funcAddr;
	RtlFreeUnicodeString(&FuncName);// �ͷ��ڴ�

	return bRet;
}
/**
�����ڴ�
*/
BOOLEAN AllocMemory(LtqData* data) {
	BOOLEAN bRet = TRUE;
	PEPROCESS process = NULL;

	PsLookupProcessByProcessId((HANDLE)data->pid, &process);
	if (process == NULL)
	{
		DbgPrint("�ͷ�ࣺ��ȡ���̶���ʧ��\n");
		return FALSE;
	}

	KAPC_STATE stack = { 0 };
	KeStackAttachProcess(process, &stack);

	PVOID BaseAddress = NULL;
	__try
	{
		ZwAllocateVirtualMemory(NtCurrentProcess(), &BaseAddress, 0, &data->size, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
		RtlZeroMemory(BaseAddress, data->size);//��ַ����
	   //RtlCopyMemory(pIoBuffer, BaseAddress, sizeof(BaseAddress));
		*(PVOID*)data = BaseAddress;
		DbgPrint("�ͷ��: �����ڴ�ɹ� ��ַ:0x%X\n", BaseAddress);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		DbgPrint("�ͷ��: �����ڴ��쳣\r\n");
		goto END;
	}

END:
	//IoFreeMdl(mdl);
	//ExFreePool(GetData);
	KeUnstackDetachProcess(&stack);
	ObDereferenceObject(process);

	return bRet;
}

/**
�ͷ��ڴ�
*/
BOOLEAN FreeMemory(LtqData* data) {
	BOOLEAN bRet = TRUE;
	PEPROCESS process = NULL;

	
	return bRet;
}

/**
��������
*/
BOOLEAN OpenProtect(LtqData* data) {
	BOOLEAN bRet = TRUE;
	for (size_t i = 0; i < sizeof(protectPid); i++)
	{
		if (protectPid[i] == 0) {
			protectPid[i] = data->pid;
			DbgPrint("�ͷ��: ���뱣��PID:%x\r\n", data->pid);
			return bRet;
		}
	}
	bRet = FALSE;
	return bRet;
}
/**
�رձ���
*/
BOOLEAN OffProtect(LtqData* data) {
	BOOLEAN bRet = TRUE;
	for (size_t i = 0; i < sizeof(protectPid); i++)
	{
		if (protectPid[i] == data->pid) {
			protectPid[i] = 0;
			DbgPrint("�ͷ��: �Ƴ�����PID:%x\r\n", data->pid);
		}
	}
	return bRet;
}

ULONG64 GetProcessModuleBase64(IN ULONG64 ProcessId, IN UNICODE_STRING ModuleName)
{
	ULONG64 ModulesBase = 0;
	NTSTATUS nStatus;
	KAPC_STATE KAPC = { 0 };
	PEPROCESS  pEProcess = NULL; //EPROCESS�ṹָ��;

	PPEB64 pPEB64 = NULL; //PEB�ṹָ��;
	PLDR_DATA_TABLE_ENTRY64 pLdrDataEntry64 = NULL; //LDR�������;
	PLIST_ENTRY64 pListEntryStart64 = NULL, pListEntryEnd64 = NULL;; //����ͷ�ڵ㡢β�ڵ�;

	__try
	{
		nStatus = PsLookupProcessByProcessId((HANDLE)ProcessId, &pEProcess);
		if (!NT_SUCCESS(nStatus) && !MmIsAddressValid(pEProcess))
		{
			return 0;
		}
		KeStackAttachProcess(pEProcess, &KAPC);

		pPEB64 = PsGetProcessPeb(pEProcess);
		pListEntryStart64 = pListEntryEnd64 = (PLIST_ENTRY64)(((PEB_LDR_DATA64*)pPEB64->Ldr)->InMemoryOrderModuleList.Flink);
		do {
			pLdrDataEntry64 = (PLDR_DATA_TABLE_ENTRY64)CONTAINING_RECORD(pListEntryStart64, LDR_DATA_TABLE_ENTRY64, InMemoryOrderLinks);
			//���DLL���� ���� ����;
			//DbgPrint("�ͷ�ࣺ Base:%p Size:%ld Name:%wZ\n", (PVOID)pLdrDataEntry64->DllBase, (ULONG)pLdrDataEntry64->SizeOfImage, &pLdrDataEntry64->BaseDllName);

			UNICODE_STRING QueryModuleName = { 0 };
			RtlInitUnicodeString(&QueryModuleName, (PWCHAR)pLdrDataEntry64->BaseDllName.Buffer);
			if (RtlEqualUnicodeString(&ModuleName, &QueryModuleName, TRUE))
			{
				ModulesBase = (ULONG64)pLdrDataEntry64->DllBase;
				goto exit;
			}
			pListEntryStart64 = (PLIST_ENTRY64)pListEntryStart64->Flink;

		} while (pListEntryStart64 != pListEntryEnd64);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		DbgPrint("�ͷ�ࣺȡģ���쳣\n");

		KeUnstackDetachProcess(&KAPC);
		ObDereferenceObject(pEProcess);
		return ModulesBase;
	}

exit:
	KeUnstackDetachProcess(&KAPC);
	ObDereferenceObject(pEProcess);
	return ModulesBase;
}
ULONG64 GetProcessModuleBase32(IN ULONG64 ProcessId, IN UNICODE_STRING ModuleName)
{
	ULONG64 ModulesBase = 0;
	NTSTATUS nStatus;
	KAPC_STATE KAPC = { 0 };
	PEPROCESS  pEProcess = NULL; //EPROCESS�ṹָ��;


	PPEB32 pPEB32 = NULL; //PEB�ṹָ��;
	PLDR_DATA_TABLE_ENTRY32 pLdrDataEntry32 = NULL; //LDR�������;
	PLIST_ENTRY32 pListEntryStart32 = NULL, pListEntryEnd32 = NULL; //����ͷ�ڵ㡢β�ڵ�;


	nStatus = PsLookupProcessByProcessId((HANDLE)ProcessId, &pEProcess);
	if (!NT_SUCCESS(nStatus) && !MmIsAddressValid(pEProcess))
	{
		return 0;
	}
	KeStackAttachProcess(pEProcess, &KAPC);
	__try
	{
		pPEB32 = PsGetProcessWow64Process(pEProcess);
		pListEntryStart32 = pListEntryEnd32 = (PLIST_ENTRY32)(((PEB_LDR_DATA32*)pPEB32->Ldr)->InMemoryOrderModuleList.Flink);
		do {
			pLdrDataEntry32 = (PLDR_DATA_TABLE_ENTRY32)CONTAINING_RECORD(pListEntryStart32, LDR_DATA_TABLE_ENTRY32, InMemoryOrderLinks);
			//���DLL���� ���� ����;
			//DbgPrint("�ͷ�ࣺ Base:%p Size:%ld Name:%wZ\n", (PVOID)pLdrDataEntry32->DllBase, (ULONG)pLdrDataEntry32->SizeOfImage, &pLdrDataEntry32->BaseDllName);
			UNICODE_STRING QueryModuleName = { 0 };
			RtlInitUnicodeString(&QueryModuleName, (PWCHAR)pLdrDataEntry32->BaseDllName.Buffer);
			if (RtlEqualUnicodeString(&ModuleName, &QueryModuleName, TRUE))
			{
				ModulesBase = (ULONG64)pLdrDataEntry32->DllBase;
				goto exit;
			}
			pListEntryStart32 = (PLIST_ENTRY32)pListEntryStart32->Flink;

		} while (pListEntryStart32 != pListEntryEnd32);
	}

	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		DbgPrint("�ͷ�ࣺȡģ���쳣\n");
		KeUnstackDetachProcess(&KAPC);
		ObDereferenceObject(pEProcess);
		return ModulesBase;
	}

exit:
	KeUnstackDetachProcess(&KAPC);
	ObDereferenceObject(pEProcess);
	return ModulesBase;
}

/**
读模块地址
*/
ULONG64 GetProcessModuleBaseAll(IN ULONG ProcessId, IN UNICODE_STRING ModuleName) {

	ULONG64 ModulesBase = 0;
	ULONG_PTR dqCurrentBase = 0;
	PEPROCESS Process = NULL;

	MEMORY_BASIC_INFORMATION baseinfo;
	MEMORY_SECTION_NAME sec;

	if (NT_SUCCESS(PsLookupProcessByProcessId(ProcessId, &Process))) {
		RtlZeroMemory(&baseinfo, sizeof(baseinfo));

		SIZE_T ret = 0;

		KeAttachProcess(Process);
		for (dqCurrentBase = 0;
			NT_SUCCESS(ZwQueryVirtualMemory(NtCurrentProcess(), (PVOID)dqCurrentBase, MemoryBasicInformation, &baseinfo, sizeof(MEMORY_BASIC_INFORMATION), &ret));
			dqCurrentBase = (ULONG_PTR)baseinfo.BaseAddress + baseinfo.RegionSize)
		{
			if (baseinfo.Type == 0x1000000 && (ULONG_PTR)baseinfo.AllocationBase == dqCurrentBase)//MEM_IMAGE  
			{
				// MemorySectionName
				if (NT_SUCCESS(ZwQueryVirtualMemory(NtCurrentProcess(), (PVOID)dqCurrentBase, (MEMORY_INFORMATION_CLASS)2, &sec, sizeof(MEMORY_SECTION_NAME), &ret)))
				{
					wchar_t* name = (wcsrchr(&sec.Name.Buffer, '\\') + 1);

					UNICODE_STRING QueryModuleName = { 0 };
					RtlInitUnicodeString(&QueryModuleName, (PWCHAR)name);

					//DbgPrint("愣头青: 地址:0x%X ,名称：%wZ ,类型：%0x%X\n", dqCurrentBase, QueryModuleName, baseinfo.Type);
					if (RtlEqualUnicodeString(&ModuleName, &QueryModuleName, TRUE))
					{
						ModulesBase = (ULONG64)dqCurrentBase;
						DbgPrint("愣头青: 读模块成功 地址:0x%X ,名称：%wZ\n", dqCurrentBase, QueryModuleName);
						KeDetachProcess();
						ObDereferenceObject(Process);
						return ModulesBase;
					}
				}
			}
		}

		KeDetachProcess();
		ObDereferenceObject(Process);
	}
	return ModulesBase;
}



ULONG64 GetModuleFuncBase64(IN ULONG64 ProcessId, IN ULONG64 ModuleAddress, IN UNICODE_STRING FuncName) {
	USHORT index = 0;
	ULONG addr, i;
	HANDLE hMod;
	PUCHAR pFuncName = NULL;

	NTSTATUS nStatus;
	KAPC_STATE KAPC = { 0 };
	PEPROCESS  pEProcess = NULL; //EPROCESS结构指针;

	ULONG64 funAddr = 0;

	IMAGE_DOS_HEADER* dosheader;
	IMAGE_OPTIONAL_HEADER64* opthdr;
	PIMAGE_EXPORT_DIRECTORY exports;
	PULONG pAddressOfFunctions;
	PULONG pAddressOfNames;
	PUSHORT pAddressOfNameOrdinals;

	if (!ProcessId || !ModuleAddress) return 0;
	__try
	{
		nStatus = PsLookupProcessByProcessId((HANDLE)ProcessId, &pEProcess);
		if (!NT_SUCCESS(nStatus) && !MmIsAddressValid(pEProcess))
		{
			return 0;
		}
		KeStackAttachProcess(pEProcess, &KAPC);

		hMod = ModuleAddress;

		dosheader = (IMAGE_DOS_HEADER*)hMod;
		opthdr = (IMAGE_OPTIONAL_HEADER64*)((BYTE*)hMod + dosheader->e_lfanew + 24);

		//查找导出表 
		exports = (PIMAGE_EXPORT_DIRECTORY)((BYTE*)dosheader + opthdr->DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress);
		pAddressOfFunctions = (ULONG*)((BYTE*)hMod + exports->AddressOfFunctions);
		pAddressOfNames = (ULONG*)((BYTE*)hMod + exports->AddressOfNames);
		pAddressOfNameOrdinals = (USHORT*)((BYTE*)hMod + exports->AddressOfNameOrdinals);

		//对比函数名 
		for (i = 0; i < exports->NumberOfNames; i++)
		{
			index = pAddressOfNameOrdinals[i];
			addr = pAddressOfFunctions[index];
			pFuncName = (PUCHAR)((BYTE*)hMod + pAddressOfNames[i]);
			addr = pAddressOfFunctions[index];

			ULONG64 pFuncAddr = (ULONG64)ModuleAddress + addr;

			ANSI_STRING AnsiStringSec = { 0 };
			RtlInitString(&AnsiStringSec, pFuncName);
			UNICODE_STRING QueryFuncName = { 0 };
			RtlAnsiStringToUnicodeString(&QueryFuncName, &AnsiStringSec, TRUE);

			//DbgPrint("愣头青: 地址:0x%X ,名称：%s 名称：%wZ \n", pFuncAddr, pFuncName, &QueryFuncName);
			if (RtlEqualUnicodeString(&QueryFuncName, &FuncName, TRUE))
			{
				funAddr = pFuncAddr;
				DbgPrint("愣头青: 读函数成功 地址:0x%X ,名称：%wZ\n", funAddr, &QueryFuncName);
				KeUnstackDetachProcess(&KAPC);
				ObDereferenceObject(pEProcess);
				return funAddr;
			}
		}
	}
	__except (1) {

		DbgPrint("愣头青：取函数异常\n");
	}

	KeUnstackDetachProcess(&KAPC);
	ObDereferenceObject(pEProcess);
	return funAddr;
}
ULONG64 GetModuleFuncBase32(IN ULONG64 ProcessId, IN ULONG64 ModuleAddress, IN UNICODE_STRING FuncName) {
	USHORT index = 0;
	ULONG addr, i;
	HANDLE hMod;
	PUCHAR pFuncName = NULL;

	NTSTATUS nStatus;
	KAPC_STATE KAPC = { 0 };
	PEPROCESS  pEProcess = NULL; //EPROCESS结构指针;

	ULONG64 funAddr = 0;

	IMAGE_DOS_HEADER* dosheader;
	IMAGE_OPTIONAL_HEADER32* opthdr;
	PIMAGE_EXPORT_DIRECTORY exports;
	PULONG pAddressOfFunctions;
	PULONG pAddressOfNames;
	PUSHORT pAddressOfNameOrdinals;


	if (!ProcessId || !ModuleAddress) return 0;
	__try
	{
		nStatus = PsLookupProcessByProcessId((HANDLE)ProcessId, &pEProcess);
		if (!NT_SUCCESS(nStatus) && !MmIsAddressValid(pEProcess))
		{
			return 0;
		}
		KeStackAttachProcess(pEProcess, &KAPC);

		hMod = ModuleAddress;

		dosheader = (IMAGE_DOS_HEADER*)hMod;
		opthdr = (IMAGE_OPTIONAL_HEADER32*)((BYTE*)hMod + dosheader->e_lfanew + 24);

		//查找导出表 
		exports = (PIMAGE_EXPORT_DIRECTORY)((BYTE*)dosheader + opthdr->DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress);
		pAddressOfFunctions = (ULONG*)((BYTE*)hMod + exports->AddressOfFunctions);
		pAddressOfNames = (ULONG*)((BYTE*)hMod + exports->AddressOfNames);
		pAddressOfNameOrdinals = (USHORT*)((BYTE*)hMod + exports->AddressOfNameOrdinals);

		//对比函数名 
		for (i = 0; i < exports->NumberOfNames; i++)
		{
			index = pAddressOfNameOrdinals[i];
			addr = pAddressOfFunctions[index];
			pFuncName = (PUCHAR)((BYTE*)hMod + pAddressOfNames[i]);
			addr = pAddressOfFunctions[index];

			ULONG64 pFuncAddr = (ULONG64)ModuleAddress + addr;

			ANSI_STRING AnsiStringSec = { 0 };
			RtlInitString(&AnsiStringSec, pFuncName);
			UNICODE_STRING QueryFuncName = { 0 };
			RtlAnsiStringToUnicodeString(&QueryFuncName, &AnsiStringSec, TRUE);

			//DbgPrint("愣头青: 地址:0x%X ,名称：%s 名称：%wZ \n", pFuncAddr, pFuncName, &QueryFuncName);
			if (RtlEqualUnicodeString(&QueryFuncName, &FuncName, TRUE))
			{
				funAddr = pFuncAddr;
				DbgPrint("愣头青: 读函数成功 地址:0x%X ,名称：%wZ\n", funAddr, QueryFuncName);
				KeUnstackDetachProcess(&KAPC);
				ObDereferenceObject(pEProcess);
				return funAddr;
			}
		}
	}
	__except (1) {

		DbgPrint("愣头青：取函数异常\n");
	}

	KeUnstackDetachProcess(&KAPC);
	ObDereferenceObject(pEProcess);
	return funAddr;
}

preCall(PVOID RegistrationContext, POB_PRE_OPERATION_INFORMATION pOperationInformation)
{
	HANDLE pid = PsGetProcessId((PEPROCESS)pOperationInformation->Object);
	char szProcName[16] = { 0 };
	UNREFERENCED_PARAMETER(RegistrationContext);
	strcpy(szProcName, GetProcessImageNameByProcessID((ULONG)pid));
	BOOLEAN flag = FALSE;
	for (size_t i = 0; i < sizeof(protectPid); i++)
	{
		if (protectPid[i] == (ULONG64)pid) {
			flag = TRUE;
			//DbgPrint("愣头青: 操作自身，跳过保护！\r\n");
		}
	}

	if (flag)//拦截
	{
		ACCESS_MASK old权限 = pOperationInformation->Parameters->CreateHandleInformation.OriginalDesiredAccess;
		ACCESS_MASK old1 = old权限;
		ACCESS_MASK new1 = pOperationInformation->Parameters->CreateHandleInformation.DesiredAccess;
		//排除 PROCESS_VM_READ 权限
		old权限 &= ~PROCESS_VM_READ;//内存读
		old权限 &= ~PROCESS_VM_WRITE;//内存写
		old权限 &= ~PROCESS_TERMINATE; //进程终止
		//old权限 &= ~PROCESS_VM_OPERATION;//openprocess
		//返回我们修改过的权限 OpenProcess
		pOperationInformation->Parameters->CreateHandleInformation.DesiredAccess = old权限;
		DbgPrint("old权限=%x 新权限=%X\n", old权限, new1);

		//DbgPrint("愣头青:进入HOOK保护，拦截权限。操作进程=%s -", szProcName);
		/*if (pOperationInformation->Operation == OB_OPERATION_HANDLE_CREATE)
		{
			if ((pOperationInformation->Parameters->CreateHandleInformation.OriginalDesiredAccess & PROCESS_TERMINATE) == PROCESS_TERMINATE)
			{
				//Terminate the process, such as by calling the user-mode TerminateProcess routine..
				pOperationInformation->Parameters->CreateHandleInformation.DesiredAccess &= ~PROCESS_TERMINATE;
			}
			if ((pOperationInformation->Parameters->CreateHandleInformation.OriginalDesiredAccess & PROCESS_VM_OPERATION) == PROCESS_VM_OPERATION)
			{
				//Modify the address space of the process, such as by calling the user-mode WriteProcessMemory and VirtualProtectEx routines.
				pOperationInformation->Parameters->CreateHandleInformation.DesiredAccess &= ~PROCESS_VM_OPERATION;
			}
			if ((pOperationInformation->Parameters->CreateHandleInformation.OriginalDesiredAccess & PROCESS_VM_READ) == PROCESS_VM_READ)
			{
				//Read to the address space of the process, such as by calling the user-mode ReadProcessMemory routine.
				pOperationInformation->Parameters->CreateHandleInformation.DesiredAccess &= ~PROCESS_VM_READ;
			}
			if ((pOperationInformation->Parameters->CreateHandleInformation.OriginalDesiredAccess & PROCESS_VM_WRITE) == PROCESS_VM_WRITE)
			{
				//Write to the address space of the process, such as by calling the user-mode WriteProcessMemory routine.
				pOperationInformation->Parameters->CreateHandleInformation.DesiredAccess &= ~PROCESS_VM_WRITE;
			}
		}*/
	}
	return OB_PREOP_SUCCESS;
}
/**
内存保护
*/
NTSTATUS ProtectProcess(BOOLEAN Enable)
{
	if (Enable) {
		OB_CALLBACK_REGISTRATION obReg;
		OB_OPERATION_REGISTRATION opReg;

		memset(&obReg, 0, sizeof(obReg));
		obReg.Version = ObGetFilterVersion();
		obReg.OperationRegistrationCount = 1;
		obReg.RegistrationContext = NULL;
		RtlInitUnicodeString(&obReg.Altitude, L"321000");
		memset(&opReg, 0, sizeof(opReg)); //初始化结构体变量

		//下面请注意这个结构体的成员字段的设置
		opReg.ObjectType = PsProcessType;
		opReg.Operations = OB_OPERATION_HANDLE_CREATE | OB_OPERATION_HANDLE_DUPLICATE;

		opReg.PreOperation = (POB_PRE_OPERATION_CALLBACK)&preCall; //在这里注册一个回调函数指针

		obReg.OperationRegistration = &opReg; //注意这一条语句

		NTSTATUS status = ObRegisterCallbacks(&obReg, &obHandle); //在这里注册回调函数
		DbgPrint("愣头青：HOOK保护：0x%x\n", obHandle);
		return status;
	}
	else {
		if (obHandle > 0) {
			DbgPrint("愣头青：关闭HOOK保护\n");
			ObUnRegisterCallbacks(obHandle);
		}
		return STATUS_SUCCESS;
	}
}

char*
GetProcessImageNameByProcessID(ULONG ulProcessID)
{
	NTSTATUS  Status;
	PEPROCESS  EProcess = NULL;


	Status = PsLookupProcessByProcessId((HANDLE)ulProcessID, &EProcess);    //EPROCESS

	//通过句柄获取EProcess
	if (!NT_SUCCESS(Status))
	{
		return FALSE;
	}
	ObDereferenceObject(EProcess);
	//通过EProcess获得进程名称
	return (char*)PsGetProcessImageFileName(EProcess);

}


BOOLEAN RemoveSystemModule(PDRIVER_OBJECT pDriverObj)
{
	OSVERSIONINFOW os = { 0 };
	RtlGetVersion(&os);
	if (os.dwMajorVersion == 0x6 && os.dwMinorVersion == 0x1 && os.dwBuildNumber == 7601)
	{
		//确定是win7 64位版本 可以抹除驱动信息
		g_MiProcessLoaderEntry = GetMiProcessLoaderEntryAddr();
		if (g_MiProcessLoaderEntry)
		{
			g_MiProcessLoaderEntry((PKLDR_DATA_TABLE_ENTRY)pDriverObj->DriverSection, FALSE);

			PKLDR_DATA_TABLE_ENTRY m_Self = (PKLDR_DATA_TABLE_ENTRY)pDriverObj->DriverSection;

			PIMAGE_NT_HEADERS64 pHeaders = (PIMAGE_NT_HEADERS64)((PUCHAR)m_Self->DllBase + ((PIMAGE_DOS_HEADER)m_Self->DllBase)->e_lfanew);

			for (ULONG t = 0; t < pHeaders->OptionalHeader.NumberOfRvaAndSizes; t++)
			{
				RtlZeroMemory(&pHeaders->OptionalHeader.DataDirectory[t], sizeof(IMAGE_DATA_DIRECTORY));
			}
			PIMAGE_SECTION_HEADER pSection = (PIMAGE_SECTION_HEADER)(pHeaders->FileHeader.SizeOfOptionalHeader + (PUCHAR)&pHeaders->OptionalHeader);
			for (ULONG i = 0; i < pHeaders->FileHeader.NumberOfSections; i++)
			{
				RtlZeroMemory(pSection, sizeof(IMAGE_SECTION_HEADER));
				pSection += 1;
			}
		}
		//破坏驱动对象特征
		pDriverObj->DriverSection = NULL;
		pDriverObj->DeviceObject = NULL;
		pDriverObj->DriverStart = NULL;
		pDriverObj->DriverSize = NULL;
		pDriverObj->DriverInit = NULL;
	}
	return TRUE;
}


pMiProcessLoaderEntry GetMiProcessLoaderEntryAddr()
{
	pMiProcessLoaderEntry MiProcessLoaderEntryAddr = NULL;
	MODULE_INFO ModuleInfo = { 0 };
	UCHAR* pAddr = NULL;
	UCHAR* pEndAddr = NULL;
	int i = 0;
	//<<begin:ntoskrnl.exe|szName
	char szName[13];//ntoskrnl.exe
	*(unsigned short*)&szName[1] = (unsigned short)0x6f74;
	*(unsigned short*)&szName[7] = (unsigned short)0x2e6c;
	szName[0] = (unsigned char)0x6e;
	szName[11] = (unsigned char)0x65;
	szName[3] = (unsigned char)0x73;
	*(unsigned short*)&szName[9] = (unsigned short)0x7865;
	szName[4] = (unsigned char)0x6b;
	szName[12] = (unsigned char)0x00;
	*(unsigned short*)&szName[5] = (unsigned short)0x6e72;
	//end>>
	__try
	{
		do
		{
			if (!GetTargetModuleInfo(&ModuleInfo, szName))
			{
				break;
			}
			pEndAddr = (UCHAR*)(ModuleInfo.uBaseAddr + ModuleInfo.uBaseSize);
			for (pAddr = (UCHAR*)ModuleInfo.uBaseAddr; pAddr < pEndAddr; pAddr++)
			{
				if (MmIsAddressValid((PVOID)(pAddr)))
				{
					if (*pAddr == g_szMiProcessLoaderEntry[0])
					{
						for (i = 1; i < sizeof(g_szMiProcessLoaderEntry); i++)
						{
							if (*(pAddr + i) == g_szMiProcessLoaderEntry[i])
							{
								continue;
							}
							else
							{
								break;
							}
						}
						if (i == sizeof(g_szMiProcessLoaderEntry))
						{
							MiProcessLoaderEntryAddr = (pMiProcessLoaderEntry)pAddr;
							//DbgPrint("Hello:MiProcessLoaderEntry Addr =%p\r\n", MiProcessLoaderEntryAddr);
							break;
						}
					}
				}
			}
		} while (FALSE);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		DbgPrint("愣头青: pMiProcessLoaderEntry 异常\r\n");
	}
	return MiProcessLoaderEntryAddr;
}


BOOLEAN GetTargetModuleInfo(PMODULE_INFO pModuleInfo, char* szModuleName)
{
	BOOLEAN bRet = FALSE;
	PRTL_PROCESS_MODULES buf = NULL;
	ULONG ulength = 0;

	__try
	{
		do
		{
			ZwQuerySystemInformation(SystemModuleInformation, NULL, 0, &ulength);
			if (!ulength)
			{
				break;
			}

			buf = (PRTL_PROCESS_MODULES)ExAllocatePool(NonPagedPool, ulength);
			if (!buf)
			{
				break;
			}
			if (!NT_SUCCESS(ZwQuerySystemInformation(SystemModuleInformation, buf, ulength, &ulength)))
			{
				ExFreePool(buf);
				break;
			}

			for (ULONG i = 0; i < buf->NumberOfModules; i++)
			{
				if (strstr((char*)buf->Modules[i].FullPathName, szModuleName))
				{
					pModuleInfo->uBaseAddr = (ULONG64)buf->Modules[i].ImageBase;
					pModuleInfo->uBaseSize = buf->Modules[i].ImageSize;
					bRet = TRUE;
					break;
				}
			}
			ExFreePool(buf);
		} while (FALSE);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		DbgPrint("愣头青: 取进程模块地址异常\r\n");
	}
	return bRet;
}



/************************************************************************
函数名称：LoadImageNotifyRoutine
函数功能：模块加载回调通知
************************************************************************/
VOID LoadImageNotifyRoutine
(
	__in_opt PUNICODE_STRING  FullImageName,
	__in HANDLE  ProcessId,
	__in PIMAGE_INFO  ImageInfo
)
{
	PVOID pDrvEntry;
	char szFullImageName[260] = { 0 };
	if (FullImageName != NULL && MmIsAddressValid(FullImageName))
	{
		if (ProcessId == 0)
		{
			DbgPrint("[LoadImageNotifyX64]%wZ\n", FullImageName);
			pDrvEntry = GetDriverEntryByImageBase(ImageInfo->ImageBase);
			DbgPrint("[LoadImageNotifyX64]DriverEntry: %p\n", pDrvEntry);
			UnicodeToChar(FullImageName, szFullImageName);
			if (strstr(_strlwr(szFullImageName), "tesxnginx.sys"))
			{
				DbgPrint("Deny load [tesxnginx.SYS]");
				//禁止加载tesxnginx.sys
				DenyLoadDriver(pDrvEntry);
			}
		}
	}
}

PVOID GetDriverEntryByImageBase(PVOID ImageBase)
{
	PIMAGE_DOS_HEADER pDOSHeader;
	PIMAGE_NT_HEADERS64 pNTHeader;
	PVOID pEntryPoint;
	pDOSHeader = (PIMAGE_DOS_HEADER)ImageBase;
	pNTHeader = (PIMAGE_NT_HEADERS64)((ULONG64)ImageBase + pDOSHeader->e_lfanew);
	pEntryPoint = (PVOID)((ULONG64)ImageBase + pNTHeader->OptionalHeader.AddressOfEntryPoint);
	return pEntryPoint;
}

void DenyLoadDriver(PVOID DriverEntry)
{
	UCHAR fuck[] = "\xB8\x22\x00\x00\xC0\xC3";
	VxkCopyMemory(DriverEntry, fuck, sizeof(fuck));
}

VOID UnicodeToChar(PUNICODE_STRING dst, char* src)
{
	ANSI_STRING string;
	RtlUnicodeStringToAnsiString(&string, dst, TRUE);
	strcpy(src, string.Buffer);
	RtlFreeAnsiString(&string);
}

BOOLEAN VxkCopyMemory(PVOID pDestination, PVOID pSourceAddress, SIZE_T SizeOfCopy)
{
	PMDL pMdl = NULL;
	PVOID pSafeAddress = NULL;
	pMdl = IoAllocateMdl(pSourceAddress, (ULONG)SizeOfCopy, FALSE, FALSE, NULL);
	if (!pMdl) return FALSE;
	__try
	{
		MmProbeAndLockPages(pMdl, KernelMode, IoReadAccess);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		IoFreeMdl(pMdl);
		return FALSE;
	}
	pSafeAddress = MmGetSystemAddressForMdlSafe(pMdl, NormalPagePriority);
	if (!pSafeAddress) return FALSE;

	KIRQL irql = WPOF();
	RtlCopyMemory(pDestination, pSafeAddress, SizeOfCopy);
	WPON(irql);


	MmUnlockPages(pMdl);
	IoFreeMdl(pMdl);
	return TRUE;
}

/***********************************
*BOOL LoadRometeDll(DWORD dwProcessId,LPTSTR lpszLibName)
*功能：通过创建远程线程给其他进程加载DLL
*参数：DWORD dwProcessId 目标进程PID
* LPTSTR lpszLibName Dll的路径
*返回：是否成功
************************************/
/*
BOOLEAN LoadRometeDll(LtqData* data)
{
	HANDLE hThread = NULL;

	BOOLEAN bRet = TRUE;
	PEPROCESS process = NULL;

	if (NT_SUCCESS(PsLookupProcessByProcessId((HANDLE)data->pid, &process))) {

		KAPC_STATE stack = { 0 };
		KeStackAttachProcess(process, &stack);
		//判断地址是否有效
		if (MmIsAddressValid((PVOID)(data->address))) {
			DbgPrint("愣头青：地址校验通过\n");

			PVOID buff = NULL;
			buff = ExAllocatePoolWithTag(NonPagedPool, data->size, 'Sys');
			PVOID BaseAddress = NULL;
			__try {
				ZwAllocateVirtualMemory(NtCurrentProcess(), &BaseAddress, 0, &data->size, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
				//读取远程CALL地址
			
				wchar_t* name = "user32.dll";
				UNICODE_STRING QueryModuleName = { 0 };
				RtlInitUnicodeString(&QueryModuleName, (PWCHAR)name);

				DbgPrint("愣头青： ModuleName:%wZ\n", &QueryModuleName);
				ULONG64 module = GetProcessModuleBase64((ULONG)data->pid, QueryModuleName);
				if (module <= 0) {
					DbgPrint("愣头青：枚举64PEB失败,开始枚举32PEB。\n");
					module = GetProcessModuleBase32((ULONG)data->pid, QueryModuleName);
				}

				DbgPrint("愣头青：user32.dll地址：0x%x\n", module);

				ULONG64 CallWindowProcA_Addr = 0;
				//创建远程线程，并通过远程线程调用用户的DLL文件
				hThread = CreateRemoteThread(process, NULL, 0, CallWindowProcA_Addr, (PVOID)BaseAddress, 0, NULL);
				if (hThread == NULL) {}
					
				//等待远程线程终止
				WaitForSingleObject(hThread, INFINITE);
				bResult = TRUE;
			}__except (1) {

			}
				//关闭句柄
				if (BaseAddress != NULL)
						//VirtualFreeEx(hProcess,(PVOID)pszLibFileRemote,0,MEM_RELEASE);
				if (hThread != NULL)
					CloseHandle(hThread);
				if (process != NULL)
					ObDereferenceObject(process);
			
		}
	}

	
	return bResult;
}*/

char* join(const char* a, const char* b) {
	char* c = (char*)ExAllocatePool(NonPagedPool, strlen(a) + strlen(b) + 1);
	if (c == NULL)
	{
		DbgPrint("愣头青：分配空间失败");
		return NULL;
	}
	char* tempc = c;
	while (*a != '\0') {
		*c++ = *a++;
	}
	while ((*c++ = *b++) != '\0') {
		;

	}
	return tempc;
}
long change_uint(long a, long b, long c, long d) {
	long address = 0;
	address |= d << 24;
	address |= c << 16;
	address |= b << 8;
	address |= a;
	return address;
}

VOID Post(IN ULONG IP1, IN ULONG IP2, IN ULONG IP3, IN ULONG IP4, IN const char* url, IN const char* path, IN ULONG Port, IN const char* PostData, IN const char* Other, OUT PVOID RecvBuffer, IN ULONG RecvSize)
{
	if (MmIsAddressValid((PVOID)PostData) == FALSE)
		return;
	char DataBuffer[100] = "";
	char* HttpBuffer = join("POST /", path);
	HttpBuffer = join(HttpBuffer, " HTTP/1.1\n");
	HttpBuffer = join(HttpBuffer, "Host: ");
	HttpBuffer = join(HttpBuffer, url);
	HttpBuffer = join(HttpBuffer, "\n");
	HttpBuffer = join(HttpBuffer, "Proxy-Connection: keep-alive\n");
	HttpBuffer = join(HttpBuffer, "User-Agent: Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36 SE 2.X MetaSr 1.0\n");
	sprintf(DataBuffer, "Content-Length: %d\n", (int)strlen(PostData));
	HttpBuffer = join(HttpBuffer, DataBuffer);
	HttpBuffer = join(HttpBuffer, Other);
	HttpBuffer = join(HttpBuffer, "Content-Type: application/x-www-form-urlencoded\n\n");

	HttpBuffer = join(HttpBuffer, PostData);


	NTSTATUS 		status = STATUS_SUCCESS;
	SOCKADDR_IN 	LocalAddress = { 0, };
	SOCKADDR_IN 	RemoteAddress = { 0, };

	PWSK_SOCKET TcpSocket = CreateSocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, WSK_FLAG_CONNECTION_SOCKET);
	if (TcpSocket == NULL) {
		DbgPrint("愣头青：创建SOCKET失败\n");
		return;
	}

	LocalAddress.sin_family = AF_INET;
	LocalAddress.sin_addr.s_addr = INADDR_ANY;
	status = Bind(TcpSocket, (PSOCKADDR)&LocalAddress);
	if (!NT_SUCCESS(status)) {
		DbgPrint("愣头青：绑定失败 0x%08X\n", status);
		CloseSocket(TcpSocket);
		return;
	}

	RemoteAddress.sin_family = AF_INET;
	RemoteAddress.sin_addr.s_addr = change_uint(IP1, IP2, IP3, IP4);
	RemoteAddress.sin_port = HTON_SHORT(Port);

	status = Connect(TcpSocket, (PSOCKADDR)&RemoteAddress);
	if (!NT_SUCCESS(status)) {
		DbgPrint("愣头青：链接目标服务器失败 0x%08X\n", status);
		CloseSocket(TcpSocket);
		return;
	}

	Send(TcpSocket, HttpBuffer, strlen(HttpBuffer), 0);
	if (MmIsAddressValid((PVOID)RecvBuffer))
	{
		Receive(TcpSocket, RecvBuffer, RecvSize, 0);
		DbgPrint("愣头青：取返回数据 内容：%s 长度：%d \n", RecvBuffer, RecvSize);
	

	}
	CloseSocket(TcpSocket);
	DbgPrint("愣头青：请求完毕！\n");
}
