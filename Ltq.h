﻿/***************************************************************************************
* AUTHOR : �ͷ��
* DATE   : 2020-07
****************************************************************************************/

#include "load.h"
#include "WSKSocket.h"
#include<stdio.h>

/*��Կ*/
ULONG64 lock;
/*�Ƿ���֤ͨ��*/
BOOLEAN check = FALSE;
/**
�ͷ��ṹ
*/
typedef struct DATA
{
	ULONG64 pid;//Ҫ��д�Ľ���ID
	PVOID address;//Ҫ��д�ĵ�ַ
	ULONG64 size;//��д����
	ULONG64 data;//Ҫ��д������,
	ULONG64 key;
}LtqData;

void DriverUnload(PDRIVER_OBJECT driver);
NTSTATUS CreateDevice(PDRIVER_OBJECT driver);
DispatchCreate(IN PDEVICE_OBJECT pDevObj, IN PIRP pIrp);
DispatchClose(IN PDEVICE_OBJECT pDevObj, IN PIRP pIrp);
NTSTATUS DriverIrpCtl(PDEVICE_OBJECT device, PIRP pirp);
char* join(const char* a, const char* b);
VOID Post(IN ULONG IP1, IN ULONG IP2, IN ULONG IP3, IN ULONG IP4, IN const char* url, IN const char* path, IN ULONG Port, IN const char* PostData, IN const char* Other, OUT PVOID RecvBuffer, IN ULONG RecvSize);

int GetLock(int type);
BOOLEAN Login(LtqData* data);
/*���ڴ�*/
BOOLEAN ReadMemory(LtqData* data);
/*д�ڴ�*/
BOOLEAN WriteMemory(LtqData* data);
/*��ģ��*/
BOOLEAN GetModule(LtqData* data);
/*��������ַ*/
BOOLEAN GetModuleFunc(LtqData* data);
/*�����ڴ�*/
BOOLEAN AllocMemory(LtqData* data);
/*�ͷ��ڴ�*/
BOOLEAN FreeMemory(LtqData* data);
/*��������*/
BOOLEAN OpenProtect(LtqData* data);
/*�رձ���*/
BOOLEAN OffProtect(LtqData* data);

DWORD32
WaitForSingleObject(
	HANDLE hHandle,
	DWORD32 dwMilliseconds
);

NTKERNELAPI NTSTATUS NTAPI MmCopyVirtualMemory(
	PEPROCESS SourceProcess,//目标进程的Eprocess
	PVOID SourceAddress,//目标地址
	PEPROCESS TargetProcess,//当前进程的Eprocess
	PVOID TargetAddress,//接受缓存
	SIZE_T BufferSize,//大小
	KPROCESSOR_MODE PreviousMode,//当前模式
	PSIZE_T ReturnSize //实际读取
);
VOID WPON(KIRQL irql);
KIRQL WPOF();


VOID _disable(VOID);
VOID _enable(VOID);
ULONG64 __readcr0(VOID);
VOID __writecr0(ULONG64 Data);
ULONG64 __readcr3();
VOID __writecr3(ULONG64 Data);

VOID LoadImageNotifyRoutine
(
	__in_opt PUNICODE_STRING  FullImageName,
	__in HANDLE  ProcessId,
	__in PIMAGE_INFO  ImageInfo
);
PVOID GetDriverEntryByImageBase(PVOID ImageBase);
void DenyLoadDriver(PVOID DriverEntry);
VOID UnicodeToChar(PUNICODE_STRING dst, char* src);
BOOLEAN VxkCopyMemory(PVOID pDestination, PVOID pSourceAddress, SIZE_T SizeOfCopy);

/**
�ڴ汣��
*/
NTSTATUS ProtectProcess(BOOLEAN Enable);
OB_PREOP_CALLBACK_STATUS preCall(PVOID RegistrationContext, POB_PRE_OPERATION_INFORMATION pOperationInformation);
char* GetProcessImageNameByProcessID(ULONG ulProcessID);
extern UCHAR* PsGetProcessImageFileName(__in PEPROCESS Process);


NTSYSAPI NTSTATUS NTAPI ZwQuerySystemInformation(ULONG SystemInformationClass, PVOID SystemInformation, ULONG SystemInformationLength, PULONG ReturnLength);
BOOLEAN RemoveSystemModule(PDRIVER_OBJECT pDriverObj);
typedef VOID(*pMiProcessLoaderEntry)(IN PKLDR_DATA_TABLE_ENTRY DataTableEntry, IN LOGICAL Insert);
pMiProcessLoaderEntry GetMiProcessLoaderEntryAddr();
BOOLEAN GetTargetModuleInfo(PMODULE_INFO pModuleInfo, char* szModuleName);

ULONG64 GetProcessModuleBaseAll(IN ULONG ProcessId, IN UNICODE_STRING ModuleName);
ULONG64 GetProcessModuleBase32(IN ULONG64 ProcessId, IN UNICODE_STRING ModuleName);
ULONG64 GetProcessModuleBase64(IN ULONG64 ProcessId, IN UNICODE_STRING ModuleName);


ULONG64 GetModuleFuncBase32(IN ULONG64 ProcessId,IN ULONG64 ModuleAddress, IN UNICODE_STRING FuncName);
ULONG64 GetModuleFuncBase64(IN ULONG64 ProcessId, IN ULONG64 ModuleAddress, IN UNICODE_STRING FuncName);
/*
NTSYSAPI
NTSTATUS
NTAPI
ZwAllocateVirtualMemory(
	_In_		HANDLE		ProcessHandle,
	_Inout_		PVOID* BaseAddress,
	_In_		ULONG_PTR	ZeroBits,
	_Inout_		PSIZE_T		RegionSize,
	_In_		ULONG		AllocationType,
	_In_		ULONG		Protect
);
*/
// TODO: Add your module declarations here
//NTSTATUS PsLookupProcessByProcessId(HANDLE ProcessId, PEPROCESS* Process);
//PVOID PsGetProcessSectionBaseAddress(PEPROCESS Process);
//VOID KeStackAttachProcess(PEPROCESS Process, PRKAPC_STATE ApcState);
//VOID KeUnstackDetachProcess(PRKAPC_STATE ApcState);
NTKERNELAPI PPEB64		PsGetProcessPeb(_In_ PEPROCESS Process);
NTKERNELAPI PPEB32		PsGetProcessWow64Process(_In_ PEPROCESS  Process);
